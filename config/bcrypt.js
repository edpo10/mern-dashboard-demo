const bcrypt = require('bcrypt');

const hashPassword = async (password, saltRounds) => {
    return await bcrypt.hash(password, saltRounds);
};

const comparePassword = async () => {
    return await bcrypt.compare(password, user.passwordHash);
};

module.exports = {
    hashPassword,
    comparePassword
}
