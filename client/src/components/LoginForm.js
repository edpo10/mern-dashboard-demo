import React, { Component } from 'react';
import {
    BrowserRouter as Router,
    Switch,
    Route,
    Link
} from "react-router-dom";

import './LoginForm.css';

class LoginForm extends Component {
    render() {
        return (
            <div className="LoginForm">
                <h1>Login</h1>
                <form action="">
                    <div>
                        <label htmlFor="email">Email</label>
                        <input type="text" name="name" id="name" />
                    </div>
                    <div>
                        <label htmlFor="email">Password</label>
                        <input type="password" name="password" id="password" />
                    </div>
                    <button type="submit">Login</button>
                    <div>
                        <Link to="/register">Create an account</Link>
                    </div>
                </form>
            </div>
        );
    }
}

export default LoginForm;
