const express = require('express');
//const mongoose = require('mongoose');
const app = express();

// Import project file
const loginRouter = require('./routes/login');
const registerRouter = require('./routes/register');

// Middleware
app.use(express.json());
app.use('/login', loginRouter);
app.use('/register', registerRouter);

// Routes
app.get('/', (req, res) => {
    res.send('<h1>This is my homepage</h1>');
});

const PORT = process.env.PORT || 5000;


// Server
app.listen(PORT, () => console.log(`Running on port ${PORT}`));