const fs = require('fs');
const path = require('path');
const express = require('express');

const router = express.Router();

const bcryptConfig = require('../config/bcrypt');

router.get('/', (req, res) => {
    res.send('Get registration');
});

// User registration
router.post('/', (req, res) => {
    const userData = req.body;
    fs.readFile(path.join(__dirname, '../models/Users.json'), (err, data) => {
        const dataStr = data.toString();
        const jsonData = JSON.parse(dataStr);
        const isRegistered = jsonData.find((element) => element.email === userData.email);
        if (isRegistered === undefined) {
            bcryptConfig.hashPassword(userData.password, 10)
                .then((hash) => {
                    userData.password = hash;
                    jsonData.push(userData);
                    fs.writeFile(path.join(__dirname, '../models/Users.json'), JSON.stringify(jsonData), (err) => {
                        if (err) throw err;
                        res.json({ msg: 'New user registered' });
                    })
                })
                .catch(err);
        } else {
            res.json({ msg: 'User already registered' });
        }
    });
});

module.exports = router;